#include <Adafruit_NeoPixel.h>

//Pins for Wemos D1 mini.
#define SoundPin D4
#define LEDPin D2

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(1, LEDPin, NEO_GRB + NEO_KHZ800);


int doubleInput=300;
unsigned long lastDetected, lastChanged;
boolean ledstate=false, colorstate=false,cur_op=true;
int color[3]={0,0,255}; //Start out blue
int step=1;


void setup() {
  pinMode(D1, INPUT);
  pixels.begin();
  pixels.setPixelColor(0,pixels.Color(0,0,0));
  pixels.show();
}

void ledswitch(){
  if(ledstate==false){
    pixels.setPixelColor(0, pixels.Color(color[0],color[1],color[2]));
    pixels.show();
  }else{
    pixels.setPixelColor(0,pixels.Color(0,0,0));
    pixels.show();
  }
  ledstate=!ledstate;
  delay(100);
}

void loop() {
if (digitalRead(SoundPin) == LOW) // Sensor triggered
  {
      if(millis() - lastDetected < doubleInput){
        ledswitch();
        goto triggerExit;
      }else{
        colorstate=!colorstate;
      }
    
      lastDetected=millis();
      delay(70); //avoid second triggering from vibrations or echo. Might not occur with low frequency chips.
  }

  if(millis() - lastChanged > 60){ //Move along the rainbow every 60ms, lower number for faster change.
    if(colorstate==true && ledstate==true){
      if(cur_op==true){color[step]++;}
      else{color[step]--;}
      if(color[step]==255 || color[step]==0){
        step++;
        step=step%3;
        cur_op=!cur_op;   
    }
    pixels.setPixelColor(0, pixels.Color(color[0],color[1],color[2]));
    pixels.show();
    triggerExit:
    lastChanged=millis();
  }
  }
  

}



